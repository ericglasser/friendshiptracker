'use strict';

describe('Service: avatars', function () {

  // load the service's module
  beforeEach(module('friendshipTrackerApp'));

  // instantiate service
  var avatars;
  beforeEach(inject(function (_avatars_) {
    avatars = _avatars_;
  }));

  it('should do something', function () {
    expect(!!avatars).toBe(true);
  });

});
