'use strict';

/**
 * @ngdoc service
 * @name friendshipTrackerApp.avatars
 * @description
 * # avatars
 * Constant in the friendshipTrackerApp.
 */
angular.module('friendshipTrackerApp')
  .constant('avatars', [
    	'avatar1.png',
    	'avatar2.png',
    	'avatar3.png',
    	'avatar4.png'
    ]);
