'use strict';

/**
 * @ngdoc service
 * @name friendshipTrackerApp.Users
 * @description
 * # Users
 * Service in the friendshipTrackerApp.
 */
angular.module('friendshipTrackerApp')
  .service('Users', function () {
    // Private Variables

    var index = 0;

    // Private Methods

    // Public Methods
    this.addUser = function (user) {
        user.id = index;
        index++;
        user.friends = [];
        this.data.push(user);
    };

    this.createFriendship = function (data) {
        var users = [];

        for (var key in data) {
            users.push(data[key]);
        }

        var userOne = users[0];
        var userTwo = users[1];

        this.data[parseInt(userOne.id, 10)].friends.push(userOne.id);
        this.data[parseInt(userTwo.id, 10)].friends.push(userTwo.id);

    }; 

    // Public Variables
    this.data = [];

  });
