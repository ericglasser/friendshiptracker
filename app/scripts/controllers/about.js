'use strict';

/**
 * @ngdoc function
 * @name friendshipTrackerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the friendshipTrackerApp
 */
angular.module('friendshipTrackerApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
