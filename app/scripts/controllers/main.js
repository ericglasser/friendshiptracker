'use strict';

/**
 * @ngdoc function
 * @name friendshipTrackerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the friendshipTrackerApp
 */
angular.module('friendshipTrackerApp')
  .controller('MainCtrl', function ($scope, Users, avatars) {
  	
  	$scope.users = Users.data;
  	$scope.avatars = avatars;

  	$scope.newUserActive = true;

  	$scope.data = {
  		name: null,
  		avatar: null
  	};

  	$scope.selected = {};

  	$scope.canFriend = false;

  	$scope.addUserBox = function () {
  		$scope.newUserActive = false;
  	};

  	$scope.setAvatar = function (avatar) {
  		$scope.data.avatar = avatar;
  	};

  	$scope.cancelAddUser = function () {
  		$scope.newUserActive = true;
  		clearData();
  	};

  	$scope.addUser = function () {
  		Users.addUser($scope.data);
  		clearData();
  	};

  	$scope.selectPerson = function (id, user) {
  		if ($scope.selected[id] === undefined) {
  			if (Object.keys($scope.selected).length < 2) {
  				$scope.selected[id] = user;
  			}

  			if (Object.keys($scope.selected).length === 2) {
  				$scope.canFriend = true;
  			}
  			
  		} else {
  			delete $scope.selected[id];
  		}
  	};

  	$scope.createFriendship = function () { 
  		Users.createFriendship($scope.selected);
  		clearSelections();
  	};

  	$scope.showFriends = function (data) {
  		console.log(data);
  	};

  	function clearData() {
		$scope.data = {
	  		name: null,
	  		avatar: null
  		};
  	} 


  	function clearSelections() {
		$scope.selected = {};
  	} 

  });
